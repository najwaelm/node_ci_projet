// Importation des modules nécessaires
const request = require("supertest");
const express = require("express");
const session = require("express-session");
const flash = require("connect-flash");
const portfinder = require("portfinder");
const router= require("../routes/albums_routes");
const app = express();
let server ;

// server= app.listen(3000, () => {
//     console.log("Application lancé sur le port 3000")
// });

beforeAll(async () => {
    port = await portfinder.getPortPromise();
    server = app.listen(port);
});

afterAll((done) => {
    server.close(done);
});

app.use(express.json());
app.use(
    session({
        secret:"your secret key",
        saveUninitialized: true,
        resave: true,
    })
);

app.use(flash());
app.use("/", router); 

// définition d'une suite de tests pour nos routes
describe("Albums routes",()=>{
    it("should create a new album", async() =>{
        const res = await request (server).post("/album/create").send({
            albumTitle: "Test Album",
    });
    expect(res.statusCode).toEqual(302); //302 means que la redirection s'est bien passé
    }, 60000);
});